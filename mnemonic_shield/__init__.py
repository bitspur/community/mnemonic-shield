# File: /mnemonic_shield/__init__.py
# Project: mnemonic-shield
# File Created: 17-11-2023 20:53:28
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2023
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .types import EncryptResult
from mnemonic import Mnemonic
from pyshamir import split, combine
from sh import gpg
import base64
import getpass
import hashlib
import os
import random
import re
import string
import tempfile


class MnemonicShield:
    def __init__(self, shares=5, threshold=3, language="english"):
        self.shares = shares
        self.threshold = threshold
        self.mnemonic = Mnemonic(language)
        self.longest_word_length = max([len(word) for word in self.mnemonic.wordlist])

    def encrypt(self, mnemonic: list[str]):
        gpg_seed = self._generate_seed()
        shamir_seed = self._generate_seed()
        seed = self._get_seed(gpg_seed, shamir_seed)
        (gpg_message, gpg_recipient) = self.gpg_encrypt(gpg_seed)
        shares = self.shamir_encrypt(shamir_seed)
        encrypted_mnemonic = self._encrypt_mnemonic(mnemonic, seed)
        return EncryptResult(
            gpg_message=gpg_message,
            gpg_recipient=gpg_recipient,
            shares=shares,
            encrypted_mnemonic=encrypted_mnemonic,
        )

    def decrypt(
        self, mnemonic: list[str], gpg_message: str, shares: list[str]
    ) -> list[str]:
        gpg_seed = self.gpg_decrypt(gpg_message)
        shamir_seed = self.shamir_decrypt(shares)
        seed = self._get_seed(gpg_seed, shamir_seed)
        mnemonic = self._decrypt_mnemonic(mnemonic, seed)
        return mnemonic

    def encode(self, data: str) -> list[str]:
        base64_data = base64.b64encode(data.encode()).decode()
        chunks = [base64_data[i:i+16] for i in range(0, len(base64_data), 16)]
        encoded_data: list[str] = []
        for chunk in chunks:
            padding_needed = 16 - len(chunk) % 16
            chunk += "=" * padding_needed
            encoded_data += self.mnemonic.to_mnemonic(chunk.encode('utf-8')).split(" ")
        return encoded_data

    def decode(self, mnemonic: list[str]) -> str:
        data = ""
        for i in range(0, len(mnemonic), 24):
            chunk = mnemonic[i:i+24]
            data += str(self.mnemonic.to_entropy(' '.join(chunk)).decode('utf-8', 'ignore'))
        return base64.b64decode(data).decode()

    def _get_seed(self, gpg_seed: str, shamir_seed: str) -> str:
        return gpg_seed + ":" + shamir_seed

    def _generate_seed(self, length=6) -> bytes:
        return str(
            "".join(
                random.choice(string.ascii_letters + string.digits)
                for _ in range(length)
            )
        )

    def gpg_encrypt(self, seed: str, recipient: str = None):
        message: str = gpg(
            "--armor",
            "--encrypt",
            *(["--recipient", recipient] if recipient is not None else []),
            _in=seed,
        )
        with tempfile.NamedTemporaryFile(delete=True) as f:
            f.write(message.encode())
            f.flush()
            os.fsync(f.fileno())
            f.seek(0)
            packets = gpg(
                "--list-only",
                "--list-packets",
                f.name,
                _err_to_out=True,
                _in=seed,
            )
            pubkey_matches = re.findall(r"^:pubkey.*", packets, re.MULTILINE)
            recipients: list[str] = [
                re.findall(r"[^ ]+$", match)[0] for match in pubkey_matches
            ]
            return (message, recipients[0] if len(recipients) > 0 else None)

    def gpg_decrypt(self, message: str, password: str = None) -> str:
        with tempfile.NamedTemporaryFile(delete=True) as f:
            f.write(message.encode())
            f.flush()
            os.fsync(f.fileno())
            f.seek(0)
            if not password:
                password = getpass.getpass("Enter password: ")
            return gpg(
                "--decrypt",
                f.name,
                "--batch",
                "--passphrase",
                password,
                _err_to_out=True,
            )

    def shamir_encrypt(self, data: str):
        return [
            base64.b64encode(part)
            .decode()
            .rstrip("=")
            .replace("+", "#")
            .replace("/", "!")
            .replace("=", "$")
            for part in split(bytes(data, "utf-8"), self.shares, self.threshold)
        ]

    def shamir_decrypt(self, shares: list[str]):
        return combine(
            [
                base64.b64decode(
                    (part + "=" * (-len(part) % 4))
                    .replace("#", "+")
                    .replace("!", "/")
                    .replace("$", "=")
                )
                for part in shares
            ]
        ).decode()

    def _get_wordmap(self, wordlist_keys: list[str], wordlist_values: list[str]):
        wordmap: dict[str, str] = {}
        for i, word in enumerate(wordlist_keys):
            if word not in wordlist_keys:
                raise ValueError(f"{word} not found in word list")
            wordmap[word] = wordlist_values[i]
        return wordmap

    def _sort_wordlist(self, seed: str):
        return sorted(
            self.wordlist,
            key=lambda x: (hashlib.sha512((seed + x).encode()).hexdigest(), x),
        )

    def _encrypt_mnemonic(self, mnemonic: list[str], seed: str):
        wordmap = self._get_wordmap(self.mnemonic.wordlist, self._sort_wordlist(seed))
        return [wordmap[word] for word in mnemonic]

    def _decrypt_mnemonic(self, mnemonic: list[str], seed: str):
        wordmap = self._get_wordmap(self._sort_wordlist(seed), self.mnemonic.wordlist)
        return [wordmap[word] for word in mnemonic]
