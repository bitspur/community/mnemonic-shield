# File: /mnemonic_shield/__main__.py
# Project: mnemonic-shield
# File Created: 17-11-2023 21:27:18
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2023
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import argparse
from mnemonic_shield import MnemonicShield

async def main(command, args):
    mnemonic_shield = MnemonicShield()
    if args.command == 'encode':
        file_path = args.file_path
        data = ""
        with open(file_path, 'r') as file:
            for line in file.readlines():
                data += str(line)
        print(' '.join(mnemonic_shield.encode(data)))
    elif args.command == 'decode':
        data = []
        while True:
            try:
                line = input()
                if not line:
                    break
                data.append(line)
            except EOFError:
                break
        print(mnemonic_shield.decode('\n'.join(data).split(' ')))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('command', choices=['encode', 'decode'], help='The command to execute.')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('-f', '--file-path', help='The file path to read from.')
    args = parser.parse_args()
    asyncio.run(main(args.command, args))
