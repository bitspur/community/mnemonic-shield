from typing import List


class EncryptResult:
    def __init__(
        self,
        encrypted_mnemonic: List[str],
        gpg_message: str,
        gpg_recipient: str,
        shares: List[str],
    ):
        self.encrypted_mnemonic = encrypted_mnemonic
        self.gpg_message = gpg_message
        self.gpg_recipient = gpg_recipient
        self.shares = shares
