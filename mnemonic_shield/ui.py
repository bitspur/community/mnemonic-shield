# File: /mnemonic_shield/ui.py
# Project: mnemonic-shield
# File Created: 18-11-2023 16:33:57
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2023
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from .views import Home
from gi.repository import Gtk
from gtklara import GTKlara, State
from gtklara.fragments import Button
from gtklara.widgets import Entry, FlowBox
from mnemonic_shield import MnemonicShield


class UI(GTKlara):
    def __init__(self, children=[], **kwargs):
        self.mnemonic_shield = MnemonicShield()
        self.encrypted_mnemonic = State("")
        self.decrypted_mnemonic = State("")
        self.word_entries = FlowBox(
            Entry(
                label="Input mnemonic",
                on_insert_text=self._word_entry_on_insert_text,
                on_changed=self._on_mnemonic_change,
                input_purpose=Gtk.InputPurpose.ALPHA,
                width_chars=self.mnemonic_shield.longest_word_length,
            ),
        )
        super().__init__(children, kwargs)

    def render(self):
        return Home(
            [
                self.word_entries,
                Button(
                    label="Encrypt",
                    on_clicked=self._on_encrypt_clicked,
                ),
                Button(
                    label="Decrypt",
                    on_clicked=self._on_decrypt_clicked,
                ),
            ],
        )

    def _on_mnemonic_change(self, widget):
        mnemonic = widget.get_text()
        self.encrypted_mnemonic.set(mnemonic)

    def _on_encrypt_clicked(self, widget):
        pass

    def _on_decrypt_clicked(self, widget):
        pass

    def _word_entry_on_insert_text(self, entry, text, length, position):
        if not text.isalpha() or len(entry.get_text()) > (
            self.mnemonic_shield.longest_word_length + 1
        ):
            entry.stop_emission("insert-text")
        if text == " ":
            entry = Entry(
                label="Input mnemonic",
                on_insert_text=self._word_entry_on_insert_text,
                on_changed=self._on_mnemonic_change,
                input_purpose=Gtk.InputPurpose.ALPHA,
                width_chars=self.mnemonic_shield.longest_word_length,
            )
            self.word_entries.add(entry)
            entry.grab_focus()
