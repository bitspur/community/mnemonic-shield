import SwiftUI
import Bip39

struct ContentView: View {
    @State private var selectedTab: Int = 0
    @State private var encryptedMnemonic: String = ""
    @State private var threshold: Int = 3
    @State private var revealAll: Bool = false
    @State private var wordlist: Wordlist = .english
    @State private var wordlistName: String = "English"
    @State private var shares: Int = 3
    @State private var mnemonic: [String] = [""] {
        didSet {
            encryptedMnemonic = mnemonic.joined(separator: " ")
        }
    }

    var body: some View {
        VStack {
            ScrollView {
                VStack {
                    HStack {
                        Text("Mnemonic Shield").font(.title)
                        Spacer()
                        HStack {
                            Text("Reveal All")
                            Toggle("", isOn: $revealAll).padding(.bottom, 4)
                        }
                        HStack {
                            Text("Shares:").frame(maxWidth: .infinity, alignment: .trailing)
                            Stepper(value: $shares, in: 3...10) {
                                Text("\(shares)").frame(minWidth: 15)
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        .frame(width: 99)
                        HStack {
                            Text("Threshold:").frame(maxWidth: .infinity, alignment: .trailing)
                            Stepper(value: $threshold, in: 3...shares) {
                                Text("\(threshold)").frame(minWidth: 15)
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .onChange(of: shares) { newShares in
                                if threshold > newShares {
                                    threshold = max(3, newShares)
                                }
                            }
                        }
                        .frame(width: 138)
                        Menu {
                            Button(action: {
                                wordlistName = "English"
                                wordlist = .english
                            }) {
                                Text("English")
                            }
                            Button(action: {
                                wordlistName = "Chinese"
                                wordlist = .chinese
                            }) {
                                Text("Chinese")
                            }
                        } label: {
                            Label(wordlistName, systemImage: "globe")
                        }
                        .frame(width: 100)
                    }
                    TabView(selection: $selectedTab) {
                        EncryptMnemonicView(revealAll: $revealAll, shares: $shares, threshold: $threshold, wordlist: $wordlist)
                            .tabItem {
                                Label("Encrypt", systemImage: "1.square.fill")
                            }
                            .tag(0)
                            .frame(maxHeight: .infinity)
                        DecryptMnemonicView(revealAll: $revealAll, shares: $shares, threshold: $threshold, wordlist: $wordlist)
                            .tabItem {
                                Label("Decrypt", systemImage: "2.square.fill")
                            }
                            .tag(1)
                    }
                    .tabViewStyle(.automatic)
                    .padding(.top, 10)
                }
                .padding()
            }
            .frame(minHeight: 330)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
