import SwiftUI
import Bip39

struct DecryptMnemonicView: View {
    @Binding var revealAll: Bool
    @Binding var shares: Int
    @Binding var threshold: Int
    @Binding var wordlist: Wordlist
    @State private var alertMessage: String = ""
    @State private var mnemonicDecrypted: [String] = []
    @State private var encryptedMnemonic: [String] = [""]
    @State private var shareInputs: [String]
    @FocusState private var focusedField: Int?
    @State private var alertShow: Bool = false
    @State private var revealedWordIndex: Int? = nil
    @State private var mnemonicShield: MnemonicShield

    init(revealAll: Binding<Bool>, shares: Binding<Int>, threshold: Binding<Int>, wordlist: Binding<Wordlist>) {
    _revealAll = revealAll
    _shares = shares
    _threshold = threshold
    _wordlist = wordlist
    // Ensure the initial shares are up to 10 and threshold is maxed at 5
    _mnemonicShield = State(initialValue: MnemonicShield(shares: min(shares.wrappedValue, 10), threshold: min(threshold.wrappedValue, 5), wordlist: wordlist.wrappedValue))
    _shareInputs = State(initialValue: Array(repeating: "", count: min(shares.wrappedValue, 10)))
}

    var body: some View {
        VStack {
           HStack(alignment: .top) {
               VStack {
                Text("Original Mnemonic Phrases (Decrypted)").bold().font(.headline).frame(maxWidth: .infinity, alignment: .leading)
                LazyVGrid(columns: [GridItem(.adaptive(minimum: 250))], spacing: 8) {
                  ForEach(0..<mnemonicDecrypted.count, id: \.self) { index in
                    ZStack {
                        if revealAll || revealedWordIndex == index {
                            TextField(
                                "",
                                text: Binding(
                                    get: { self.mnemonicDecrypted[index] },
                                    set: { self.mnemonicDecrypted[index] = $0 }
                                )
                            )
                            .onTapGesture {
                                self.revealedWordIndex = index // Toggle visibility on tap
                            }
                        } else {
                            SecureField(
                                "",
                                text: Binding(
                                    get: { self.mnemonicDecrypted[index] },
                                    set: { self.mnemonicDecrypted[index] = $0 }
                                )
                            )
                            .disabled(true)
                            .onTapGesture {
                                self.revealedWordIndex = index
                            }
                        }
                    }
                    .padding(2)
                    .onHover { isHovering in
                        if isHovering {
                            self.revealedWordIndex = index
                        } else {
                            self.revealedWordIndex = nil
                        }
                    }
                }
            }
                .frame(minWidth: 900, maxWidth: .infinity, alignment: .leading)
                .padding(4)
            }
            .frame(minHeight: 100)

                Divider()

                Text("GPG Message").bold().font(.headline).frame(maxWidth: .infinity, alignment: .leading)
                    .frame(minWidth: 300, alignment: .trailing)
                    .padding(4)
                }

            Divider()

            HStack(alignment: .top) {
            VStack {
            Text("Enter Encrypted Mnemonic Phrases").bold().font(.headline).frame(maxWidth: .infinity, alignment: .leading)
                  LazyVGrid(columns: [GridItem(.adaptive(minimum: 250))], spacing: 8) {
                        ForEach($encryptedMnemonic.indices, id: \.self) { index in
                            TextField(
                                "Encrypted Word \(index + 1)",
                                text: $encryptedMnemonic[index]
                            )
                            .focused($focusedField, equals: index)
                            .onChange(of: encryptedMnemonic[index]) { newValue in
                                handleEncryptedMnemonicInputChange(newValue, at: index)
                            }

                        }
                     }
                    .frame(minWidth: 900, maxWidth: .infinity, alignment: .leading)
                    .padding(4)
                }
            .frame(minHeight: 200)

                Divider()
                VStack {
              Text("Enter Shamir's Secret Shares").bold().font(.headline)
                          .foregroundColor(.white) // Keep visibility in mind for your actual color scheme
                          .frame(maxWidth: .infinity, alignment: .leading)
                          .padding()
                      ForEach(0..<shares, id: \.self) { index in  // Use shares to dynamically create text fields
                          TextField("Shamir's Secret Share \(index + 1)", text: Binding(
                              get: { shareInputs.indices.contains(index) ? shareInputs[index] : "" },
                              set: { newValue in
                                  if shareInputs.indices.contains(index) {
                                      shareInputs[index] = newValue
                                  }
                              }
                          ))
                          .padding(2)
                      }
                  }
                  .frame(minWidth: 300, alignment: .trailing)
                  .padding(4)
            }
            .frame(maxHeight: .infinity)
        }
        .alert(isPresented: $alertShow) {
            Alert(
                title: Text("Error"),
                message: Text(alertMessage),
                dismissButton: .default(Text("OK"))
            )
        }
        .onChange(of: shares) { newShares in
                let limitedShares = min(newShares, 10)
                shares = limitedShares
                if shareInputs.count != limitedShares {
                    shareInputs = Array(repeating: "", count: limitedShares)
                }
                attemptDecryption()
            }
            .onChange(of: threshold) { newThreshold in
                let limitedThreshold = min(newThreshold, 5)
                threshold = limitedThreshold
                attemptDecryption()
            }
            .onChange(of: wordlist) { newWordlist in
                mnemonicShield = MnemonicShield(shares: shares, threshold: threshold, wordlist: newWordlist)
                attemptDecryption()
            }
            .onChange(of: shareInputs) { _ in
                attemptDecryption()
            }
      }

    private func handleEncryptedMnemonicInputChange(_ newValue: String, at index: Int) {
        let trimmedValue = newValue.trimmingCharacters(in: .whitespaces)

              if trimmedValue.contains(" ") {
                  let words = trimmedValue.split(separator: " ").map(String.init)
                  encryptedMnemonic.remove(at: index)
                  encryptedMnemonic.insert(contentsOf: words, at: index)
                  return
              }

              encryptedMnemonic[index] = trimmedValue
              if trimmedValue.count >= 10 && index == encryptedMnemonic.count - 1 {
                  encryptedMnemonic.append("")
                  focusedField = encryptedMnemonic.count - 1
              }

              if trimmedValue.isEmpty && encryptedMnemonic.count > 1 && index != encryptedMnemonic.count - 1 {
                  encryptedMnemonic.remove(at: index)
                  if let focusedIndex = focusedField, focusedIndex >= index {
                      focusedField = focusedIndex - 1
                  }
              }
          }


private func updateMnemonicShield(
    shares: Int? = nil, threshold: Int? = nil, wordlist: Wordlist? = nil
) {
    let adjustedShares = shares ?? self.shares
    let adjustedThreshold = min(threshold ?? self.threshold, adjustedShares)
    mnemonicShield = MnemonicShield(
        shares: adjustedShares,
        threshold: adjustedThreshold,
        wordlist: wordlist ?? self.wordlist)
}

  private func attemptDecryption() {
    if shareInputs.filter({ !$0.isEmpty }).count >= threshold {
        decryptMnemonic()
    }
  }

  private func decryptMnemonic() {
    mnemonicDecrypted = []
    do {
        mnemonicDecrypted = try mnemonicShield.decrypt(
            mnemonic: encryptedMnemonic,
            gpgMessage: "",
            shares: shareInputs.filter { !$0.isEmpty }
        )
    } catch {
        alertMessage = "Decryption failed: \(error.localizedDescription)"
        alertShow = true
    }
}

}

// Preview
struct DecryptMnemonicView_Previews: PreviewProvider {
    static var previews: some View {
        DecryptMnemonicView(
            revealAll: .constant(false),
            shares: .constant(5),
            threshold: .constant(3),
            wordlist: .constant(.english)
        )
    }
}
