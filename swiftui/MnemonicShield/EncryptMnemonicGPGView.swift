//
//  EncryptMnemonicGPGView.swift
//  MnemonicShield
//
//  Created by Clay Risser on 11/21/23.
//

import SwiftUI

struct EncryptMnemonicGPGView: View {
  var body: some View {
    Text("GPG")
  }
}

#Preview {
  EncryptMnemonicGPGView()
}
