import Bip39
import SwiftUI

struct EncryptMnemonicView: View {
  @Binding var revealAll: Bool
  @Binding var shares: Int
  @Binding var threshold: Int
  @Binding var wordlist: Wordlist
  @FocusState private var mnemonicFocusedField: Int?
  @State private var alertMessage: String = ""
  @State private var filteredMnemonic: [String] = []
  @State private var mnemonicEncrypted: [String] = []
  @State private var mnemonicErrorIndex: Int? = nil
  @State private var mnemonicPreviousFocusedField: Int?
  @State private var mnemonicShield: MnemonicShield
  @State private var revealedWordIndex: Int? = nil
  @State private var hoveredIndex: Int? = nil
  @State private var hoveredMnemonicIndex: Int? = nil
  @State private var hoveredShareIndex: Int? = nil
  @State private var shamirShares: [String] = []
  @State private var alertShow: Bool = false {
    didSet {
      if !alertShow {
        alertMessage = ""
      }
    }
  }

  @State private var mnemonic: [String] = [""] {
    didSet {
      let newFilteredMnemonic = mnemonic.filter { !$0.isEmpty && wordlist.words.contains($0) }
      if newFilteredMnemonic != filteredMnemonic {
        filteredMnemonic = newFilteredMnemonic
        do {
          let encryptedResult = try mnemonicShield.encrypt(mnemonic: filteredMnemonic)
          mnemonicEncrypted = encryptedResult.encryptedMnemonic
          shamirShares = encryptedResult.shares
        } catch {
          alertMessage = "Encryption failed: \(error.localizedDescription)"
          alertShow = true
        }
      }
    }
  }

   init(revealAll: Binding<Bool>, shares: Binding<Int>, threshold: Binding<Int>, wordlist: Binding<Wordlist>) {
        _revealAll = revealAll
        _shares = shares
        _threshold = threshold
        _wordlist = wordlist

        // Update mnemonicShield initialization
        _mnemonicShield = State(initialValue: MnemonicShield(shares: max(3, shares.wrappedValue), threshold: max(3, min(threshold.wrappedValue, 5)), wordlist: wordlist.wrappedValue))
    }

var body: some View {
    VStack {
      HStack(alignment: .top) {
        VStack {
         LazyVGrid(columns: [GridItem(.adaptive(minimum: 250))], spacing: 8) {
            ForEach(0..<mnemonic.count, id: \.self) { index in
              TextField(
                "mnemonic",
                text: Binding(
                  get: { index < mnemonic.count ? mnemonic[index] : "" },
                  set: { if index < mnemonic.count { mnemonic[index] = $0 } }
                )
              )
              .id(index)
              .padding(2)
              .focused($mnemonicFocusedField, equals: index)
              .border(
                Color.red,
                width: mnemonicErrorIndex == index ? 3.5 : 0
              )
              .cornerRadius(mnemonicErrorIndex == index ? 4 : 0)
              .onChange(of: mnemonicFocusedField) { focusedIndex in
                if focusedIndex == index {
                  if let previousField = mnemonicPreviousFocusedField,
                    previousField < mnemonic.count
                  {
                    let word = mnemonic[previousField].trimmingCharacters(
                      in: .whitespacesAndNewlines)
                    if wordlist.words.contains(word) {
                      mnemonic[previousField] = word
                      if previousField == mnemonic.count - 1 {
                        mnemonic.append("")
                      }
                      mnemonicErrorIndex = nil
                      mnemonicPreviousFocusedField = mnemonicFocusedField
                      mnemonicFocusedField = index
                    } else if word == "" && previousField != mnemonic.count - 1 {
                      mnemonic.remove(at: previousField)
                      mnemonicPreviousFocusedField = nil
                      if let focusedField = mnemonicFocusedField, focusedField >= mnemonic.count {
                        mnemonicFocusedField = mnemonic.count - 1
                      }
                      mnemonicErrorIndex = nil
                    } else {
                      if word != "" {
                        alertMessage = "\(word) is not a valid mnemonic word"
                        alertShow = true
                        mnemonicErrorIndex = previousField
                        mnemonic[previousField] = ""
                      }
                      mnemonicPreviousFocusedField = mnemonicFocusedField
                      mnemonicFocusedField = index
                    }
                  } else {
                    mnemonic = mnemonic.filter { !$0.isEmpty }
                    mnemonic.append("")
                    mnemonicPreviousFocusedField = nil
                    if let focusedField = mnemonicFocusedField, focusedField >= mnemonic.count {
                      mnemonicFocusedField = max(0, mnemonic.count - 1)
                    } else if mnemonicFocusedField == nil {
                      mnemonicFocusedField = max(0, mnemonic.count - 1)
                    }
                    mnemonicErrorIndex = nil
                  }
                }
              }
              .onChange(of: mnemonic[index]) { newValue in
                if index < mnemonic.count {
                  if newValue.count > 10 {
                    mnemonic[index] = String(newValue.prefix(10))
                  }
                  if newValue.last == " " {
                    let word = mnemonic[index].trimmingCharacters(
                      in: .whitespacesAndNewlines)
                    if wordlist.words.contains(word) {
                      mnemonic[index] = word
                      if index == mnemonic.count - 1 {
                        mnemonic.append("")
                      }
                      mnemonicPreviousFocusedField = mnemonicFocusedField
                      mnemonicFocusedField = index + 1
                      mnemonicErrorIndex = nil
                    } else if word == "" && index != mnemonic.count - 1 {
                      mnemonic.remove(at: index)
                      mnemonicPreviousFocusedField = nil
                      mnemonicFocusedField = max(0, mnemonic.count - 1)
                      mnemonicErrorIndex = nil
                    } else {
                      if word != "" {
                        alertMessage = "\(word) is not a valid mnemonic word"
                        alertShow = true
                        mnemonicErrorIndex = index
                      }
                      mnemonic[index] = ""
                    }
                  }
                }
              }
              .onChange(of: mnemonic[index]) { newValue in
                if index < mnemonic.count {
                  if newValue.contains(" ") {
                    let words = newValue.split(separator: " ").map(String.init)
                    var newMnemonic = mnemonic[..<index]
                    var invalidWords = [String]()

                    for word in words {
                      if wordlist.words.contains(word) {
                        newMnemonic.append(word)
                      } else {
                        invalidWords.append(word)
                      }
                    }

                    if !invalidWords.isEmpty {
                      alertMessage = "Removed Invalid Words: \(invalidWords.joined(separator: ", "))"
                      alertShow = true
                    }

                    newMnemonic.append(contentsOf: mnemonic.dropFirst(index + words.count))
                    mnemonic = Array(newMnemonic)
                    mnemonicErrorIndex = nil
                    mnemonicPreviousFocusedField = nil
                    mnemonicFocusedField = index + words.count
                  }
                }
              }

            }
          }
        }
       .frame(minWidth: 900, maxWidth: .infinity, alignment: .leading)
       .padding(4)

        Divider()

          Text("GPG Message").bold().font(.headline).frame(maxWidth: .infinity, alignment: .leading)
          EncryptMnemonicGPGView()

            .frame(minWidth: 300, alignment: .trailing)
            .padding(4)
        }.frame(minHeight: 100)

        Divider()

        HStack(alignment: .top) {
          VStack {
            HStack {
              Text("Encrypted Mnemonic Phrases").bold().font(.headline).frame(
                maxWidth: .infinity, alignment: .leading)

              Spacer()

              Button(action: {
                let joinedMnemonic = mnemonicEncrypted.joined(separator: " ")
                NSPasteboard.general.clearContents()
                NSPasteboard.general.setString(joinedMnemonic, forType: .string)
              }) {
                Image(systemName: "doc.on.doc.fill")
                  .accessibilityLabel("Copy encrypted mnemonics")
              }
            }
            .padding([.top, .horizontal])

             LazyVGrid(columns: [GridItem(.adaptive(minimum: 250))], spacing:8 ) {
                 ForEach(Array(mnemonicEncrypted.enumerated()), id: \.offset) { index, encryptedWord in
                        Group {
                            if revealAll || hoveredMnemonicIndex == index {
                                TextField("", text: .constant(encryptedWord))
                                    .onHover { isHovering in
                                        hoveredMnemonicIndex = isHovering ? index : nil
                                    }
                            } else {
                                SecureField("", text: .constant(encryptedWord))
                                    .disabled(true)
                                    .onHover { isHovering in
                                        hoveredMnemonicIndex = isHovering ? index : nil
                                    }
                            }
                        }
                        .padding(2)
                    }
                }
             }
            .frame(minWidth: 900, maxWidth: .infinity, alignment: .leading)
            .padding(4)

        Divider()

         // Shamir's Secret Shares Section
        VStack(alignment: .leading) {
            Text("Shamir's Secret Shares").bold().font(.headline)
                .frame(maxWidth: .infinity, alignment: .leading)

            // Grid for Shamir's Secret Shares with a single column layout
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 250))], spacing:8 ) {
                ForEach(Array(shamirShares.enumerated()), id: \.offset) { index, share in
                    HStack {
                        if revealAll || hoveredShareIndex == index {
                            TextField("", text: .constant(share))
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                        } else {
                            SecureField("", text: .constant(share))
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .disabled(true)
                        }
                        Spacer() // Push the copy button to the end of the grid cell
                        Button(action: {
                            // Copy to clipboard functionality for macOS
                            let pasteboard = NSPasteboard.general
                            pasteboard.clearContents()
                            pasteboard.setString(share, forType: .string)
                        }) {
                            Image(systemName: "doc.on.clipboard")
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                    .padding(.horizontal)
                    .frame(maxWidth: .infinity, alignment: .leading) // Align HStack to leading
                    .onHover { isHovering in
                        hoveredShareIndex = isHovering ? index : nil
                              }
                          }
                      }
                  }
                            .frame(minWidth: 300, alignment: .trailing)
                            .padding(4)
                        }.frame(maxHeight: .infinity)
                  }
                  .alert(isPresented: $alertShow) {
                    Alert(
                      title: Text("Error"), message: Text(alertMessage),
                      dismissButton: .default(Text("Ok")))
                  }
                  .onChange(of: shares) { newValue in
                      updateMnemonicShield(shares: max(3, newValue))
                  }
                  .onChange(of: threshold) { newValue in
                      updateMnemonicShield(threshold: max(3, min(newValue, 5)))
                  }
                  .onChange(of: wordlist) { newValue in
                    updateMnemonicShield(wordlist: newValue)
          }
       }

  private func updateMnemonicShield(
    shares: Int? = nil, threshold: Int? = nil, wordlist: Wordlist? = nil
  ) {
    do {
      let adjustedThreshold =
        (threshold ?? self.threshold) > (shares ?? self.shares)
        ? (shares ?? self.shares) : (threshold ?? self.threshold)
      mnemonicShield = MnemonicShield(
        shares: shares ?? self.shares,
        threshold: adjustedThreshold,
        wordlist: wordlist ?? self.wordlist)
      let encryptedResult = try mnemonicShield.encrypt(mnemonic: filteredMnemonic)
      mnemonicEncrypted = encryptedResult.encryptedMnemonic
      shamirShares = encryptedResult.shares
    } catch {
      alertMessage = "Encryption failed: \(error.localizedDescription)"
      alertShow = true
    }
  }
}

#Preview {
  EncryptMnemonicView(
    revealAll: .constant(false), shares: .constant(3), threshold: .constant(3),
    wordlist: .constant(.english))
}


