//
//  MnemonicShield.swift
//  MnemonicShield
//
//  Created by Clay Risser on 11/21/23.
//

import Bip39
import CryptoKit
import Foundation

struct EncryptResult {
  var gpgMessage: String
  var gpgRecipient: String
  var shares: [String]
  var encryptedMnemonic: [String]
}

class MnemonicShield {
  var shares: Int
  var threshold: Int
  var wordlist: Wordlist
  var gpgSeed: String
  var sssSeed: String

  init(shares: Int = 5, threshold: Int = 3, wordlist: Wordlist = .english) {
    let gpgSeed = MnemonicShield.generateSeed()
    let sssSeed = MnemonicShield.generateSeed()
    self.shares = shares
    self.threshold = threshold
    self.wordlist = wordlist
    self.gpgSeed = gpgSeed
    self.sssSeed = sssSeed
  }

  static func generateSeed(length: Int = 6) -> String {
    return String(
      (0..<length).map { _ in
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".randomElement()!
      })
  }

  func encrypt(mnemonic: [String]) throws -> EncryptResult {
    let seed = getSeed(gpgSeed: gpgSeed, sssSeed: sssSeed)
    let (gpgMessage, gpgRecipient) = gpgEncrypt(seed: gpgSeed)
    let shares = try sssEncrypt(data: sssSeed)
    let encryptedMnemonic = try encryptMnemonic(mnemonic: mnemonic, seed: seed)
    return EncryptResult(
      gpgMessage: gpgMessage, gpgRecipient: gpgRecipient ?? "", shares: shares,
      encryptedMnemonic: encryptedMnemonic)
  }

  func decrypt(mnemonic: [String], gpgMessage: String, shares: [String]) throws -> [String] {
    let gpgSeed = gpgDecrypt(message: gpgMessage)
    let sssSeed = try sssDecrypt(shares: shares)
    let seed = getSeed(gpgSeed: gpgSeed, sssSeed: sssSeed)
    return try decryptMnemonic(mnemonic: mnemonic, seed: seed)
  }

  func getSeed(gpgSeed: String, sssSeed: String) -> String {
    return gpgSeed + ":" + sssSeed
  }

  func gpgEncrypt(seed: String, recipient: String? = nil) -> (String, String?) {
    // Implement gpg encryption here
    return ("", nil)
  }

  func gpgDecrypt(message: String, password: String? = nil) -> String {
    // Implement gpg decryption here
    return ""
  }

  func sssEncrypt(data: String) throws -> [String] {
    let data = Data(data.utf8)
    let shamirSecret = try ShamirSecret(
      data: data, threshold: self.threshold, shares: self.shares)
    let shares = try shamirSecret.split()
    return shares.map { $0.description }
  }

  func sssDecrypt(shares: [String]) throws -> String {
    let shares = try shares.map { try ShamirSecret.Share(string: $0) }
    let data = try ShamirSecret.combine(shares: shares)
    return String(data: data, encoding: .utf8) ?? ""
  }

  func getWordmap(wordlistKeys: [String], wordlistValues: [String]) throws -> [String: String] {
    guard wordlistKeys.count == wordlistValues.count else {
      throw NSError(
        domain: "", code: -1,
        userInfo: [NSLocalizedDescriptionKey: "Wordlist keys and values count mismatch"])
    }
    var wordmap: [String: String] = [:]
    for i in 0..<wordlistKeys.count {
      wordmap[wordlistKeys[i]] = wordlistValues[i]
    }
    return wordmap
  }

  func sortWordlist(seed: String) -> [String] {
    let wordlistWithHash = wordlist.words.map { word -> (String, String) in
      let hash = SHA512.hash(data: (seed + word).data(using: .utf8)!)
      return (word, hash.compactMap { String(format: "%02x", $0) }.joined())
    }
    let sortedWordlistWithHash = wordlistWithHash.sorted { (a, b) -> Bool in
      return a.1 < b.1
    }
    return sortedWordlistWithHash.map { $0.0 }
  }

  func encryptMnemonic(mnemonic: [String], seed: String) throws -> [String] {
    let wordmap = try getWordmap(
      wordlistKeys: wordlist.words, wordlistValues: sortWordlist(seed: seed))
    return mnemonic.compactMap { wordmap[$0] }
  }

  func decryptMnemonic(mnemonic: [String], seed: String) throws -> [String] {
    let wordmap = try getWordmap(
      wordlistKeys: sortWordlist(seed: seed), wordlistValues: wordlist.words)
    return mnemonic.compactMap { wordmap[$0] }
  }
}


