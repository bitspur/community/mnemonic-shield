//
//  MnemonicShieldApp.swift
//  MnemonicShield
//
//  Created by Clay Risser on 11/21/23.
//

import SwiftUI

@main
struct MnemonicShieldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
